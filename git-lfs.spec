%bcond check 1

# https://github.com/git-lfs/git-lfs
%global goipath         github.com/git-lfs/git-lfs/v3
Version:                3.6.1

%gometa

%global gobuilddir %{_builddir}/%{name}-%{version}/_build

%global common_description %{expand:
Git extension for versioning large files.}

%global golicenses      LICENSE.md
%global godocs          docs CHANGELOG.md CODE-OF-CONDUCT.md\\\
                        CONTRIBUTING.md README.md

Name:           git-lfs
Release:        %autorelease
Summary:        Git extension for versioning large files

# See LICENSE.md for details.
License:        MIT AND BSD-3-Clause
URL:            https://git-lfs.github.io/
Source0:        https://github.com/%{name}/%{name}/releases/download/v%{version}/%{name}-v%{version}.tar.gz
Source1:        README.Fedora


# Generated provides by vendor2provides.py
# https://src.fedoraproject.org/rpms/syncthing/blob/603e4e03a92a7d704d199629dd85304018e8279d/f/vendor2provides.py
Provides:       bundled(golang(github.com/alexbrainman/sspi)) = 909beea
Provides:       bundled(golang(github.com/avast/retry-go)) = 2.4.2+incompatible
Provides:       bundled(golang(github.com/davecgh/go-spew)) = 1.1.1
Provides:       bundled(golang(github.com/dpotapov/go-spnego)) = 298b63a
Provides:       bundled(golang(github.com/git-lfs/gitobj/v2)) = 2.1.1
Provides:       bundled(golang(github.com/git-lfs/go-netrc)) = f0c862d
Provides:       bundled(golang(github.com/git-lfs/pktline)) = 06e9096
Provides:       bundled(golang(github.com/git-lfs/wildmatch/v2)) = 2.0.1
Provides:       bundled(golang(github.com/hashicorp/go-uuid)) = 1.0.2
Provides:       bundled(golang(github.com/inconshreveable/mousetrap)) = 1.1.0
Provides:       bundled(golang(github.com/jcmturner/aescts/v2)) = 2.0.0
Provides:       bundled(golang(github.com/jcmturner/dnsutils/v2)) = 2.0.0
Provides:       bundled(golang(github.com/jcmturner/gofork)) = 1.0.0
Provides:       bundled(golang(github.com/jcmturner/goidentity/v6)) = 6.0.1
Provides:       bundled(golang(github.com/jcmturner/gokrb5/v8)) = 8.4.2
Provides:       bundled(golang(github.com/jcmturner/rpc/v2)) = 2.0.3
Provides:       bundled(golang(github.com/jmhodges/clock)) = 1.2.0
Provides:       bundled(golang(github.com/leonelquinteros/gotext)) = 1.5.0
Provides:       bundled(golang(github.com/mattn/go-isatty)) = 0.0.4
Provides:       bundled(golang(github.com/olekukonko/ts)) = 78ecb04
Provides:       bundled(golang(github.com/pkg/errors)) = c605e28
Provides:       bundled(golang(github.com/pmezard/go-difflib)) = 1.0.0
Provides:       bundled(golang(github.com/rubyist/tracerx)) = 7879593
Provides:       bundled(golang(github.com/spf13/cobra)) = 1.7.0
Provides:       bundled(golang(github.com/spf13/pflag)) = 1.0.5
Provides:       bundled(golang(github.com/ssgelm/cookiejarparser)) = 1.0.1
Provides:       bundled(golang(github.com/stretchr/testify)) = 1.6.1
Provides:       bundled(golang(github.com/xeipuuv/gojsonpointer)) = 4e3ac27
Provides:       bundled(golang(github.com/xeipuuv/gojsonreference)) = bd5ef7b
Provides:       bundled(golang(github.com/xeipuuv/gojsonschema)) = 6b67b3f
Provides:       bundled(golang(golang.org/x/crypto)) = 0.21.0
Provides:       bundled(golang(golang.org/x/net)) = 0.23.0
Provides:       bundled(golang(golang.org/x/sync)) = 0.1.0
Provides:       bundled(golang(golang.org/x/sys)) = 0.18.0
Provides:       bundled(golang(golang.org/x/text)) = 0.14.0
Provides:       bundled(golang(gopkg.in/yaml.v3)) = 3.0.1


# Generate man pages
BuildRequires:  /usr/bin/asciidoctor

%if %{with check}
# Tests
BuildRequires:  perl-Digest-SHA
BuildRequires:  perl-Test-Harness
# Tests require full git suite, but not generally needed.
BuildRequires:  git >= 2.32.0
%endif

Requires:       git-core >= 2.32.0

%description
Git Large File Storage (LFS) replaces large files such as audio samples,
videos, datasets, and graphics with text pointers inside Git, while
storing the file contents on a remote server.


%prep
%autosetup -p1 -n %{name}-%{version}

install -m 0755 -vd %{gobuilddir}/bin
install -m 0755 -vd "$(dirname %{gobuilddir}/src/%{goipath})"
ln -fs "$(pwd)" "%{gobuilddir}/src/%{goipath}"

# Modify tests so that they expect binaries where we build them.
sed -i -e 's!\.\./bin/!/%{gobuilddir}/bin/!g' t/Makefile
sed -i -e 's!^BINPATH=.\+!BINPATH="%{gobuilddir}/bin"!g' t/testenv.sh

%build
export GOPATH=%{gobuilddir}:%{gopath}
export GO111MODULE=off

# Enable FIPS support at build time by enabling CGO and enforcing strict FIPS runtime
export CGO_ENABLED=1
export GOEXPERIMENT=strictfipsruntime

# Build manpages first (some embedding in the executable is done.)
make man GIT_LFS_SHA=unused VERSION=unused PREFIX=unused
pushd docs
%gobuild -o %{gobuilddir}/bin/mangen man/mangen.go
%{gobuilddir}/bin/mangen
popd

%gobuild -o %{gobuilddir}/bin/git-lfs %{goipath}

# Generate completion files.
for shell in bash fish zsh; do
    %{gobuilddir}/bin/git-lfs completion ${shell} > %{name}.${shell}
done

# Build test executables
pushd %{gobuilddir}/src/%{goipath}
for cmd in t/cmd/*.go; do
    %gobuild -o "%{gobuilddir}/bin/$(basename $cmd .go)" "$cmd"
done
%gobuild -o "%{gobuilddir}/bin/git-lfs-test-server-api" t/git-lfs-test-server-api/*.go
popd

# Remove man pages from docs so they don't get installed twice.
rm -r docs/man


%install
install -Dpm0755 %{gobuilddir}/bin/git-lfs %{buildroot}%{_bindir}/%{name}
for section in 1 5 7; do
    install -d -p %{buildroot}%{_mandir}/man${section}/
    install -Dpm0644 man/man${section}/*.${section} %{buildroot}%{_mandir}/man${section}/
done
install -Dpm 0644 %{name}.bash %{buildroot}%{bash_completions_dir}/%{name}
install -Dpm 0644 %{name}.fish %{buildroot}%{fish_completions_dir}/%{name}.fish
install -Dpm 0644 %{name}.zsh  %{buildroot}%{zsh_completions_dir}/_%{name}


%post
if [ "x$(git config --type=bool --get 'fedora.git-lfs.no-modify-config')" != "xtrue" ]; then
%{_bindir}/%{name} install --system --skip-repo
fi

%preun
if [ $1 -eq 0 ] && \
   [ "x$(git config --type=bool --get 'fedora.git-lfs.no-modify-config')" != "xtrue" ]; then
    %{_bindir}/%{name} uninstall --system --skip-repo
fi
exit 0


%if %{with check}
%check
%gocheck
PATH=%{buildroot}%{_bindir}:%{gobuilddir}/bin:$PATH \
    make -C t PROVE_EXTRA_ARGS="-j$(getconf _NPROCESSORS_ONLN)"
%endif


%files
%doc README.md CHANGELOG.md docs
%license LICENSE.md
%{_bindir}/%{name}
%{_mandir}/man1/%{name}*.1*
%{_mandir}/man5/%{name}*.5*
%{_mandir}/man7/%{name}*.7*
%{bash_completions_dir}/%{name}
%{fish_completions_dir}/%{name}.fish
%{zsh_completions_dir}/_%{name}


%changelog
%autochangelog
